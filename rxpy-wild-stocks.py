# Little note on the imports here... You may need to install tkinter for your OS.
# On Ubuntu you can run apt to install the package python3-tk and you're all set.

"""General TODOS/possible future features
    TODO: Comment all functions properly.
    TODO: Better 'config' options. Maybe a config file?
    TODO: Some sort of option to enable logging.
    TODO: Figure out unit testing. Unit tests are nice.
    TODO: Go wild. This thing is just a silly experiment anyways.
"""

import random

from rx import Observable
from tkinter import *
from rx.subjects import Subject

# region Variables

# Some 'stocks'
stocks = [
    {'TCKR': 'APPL', 'PRICE': 200},
    {'TCKR': 'GOOG', 'PRICE': 130},
    {'TCKR': 'TSLA', 'PRICE': 99},
    {'TCKR': 'MSFT', 'PRICE': 150},
    {'TCKR': 'INTL', 'PRICE': 70},
    {'TCKR': 'ADSK', 'PRICE': 45},
    {'TCKR': 'AVGO', 'PRICE': 0},
    {'TCKR': 'AMZN', 'PRICE': 250},
    {'TCKR': 'HOLX', 'PRICE': 0},
    {'TCKR': 'KLAC', 'PRICE': 88},
    {'TCKR': 'NFLX', 'PRICE': 256},
    {'TCKR': 'MXIM', 'PRICE': 50},
    {'TCKR': 'NVDA', 'PRICE': 100},
    {'TCKR': 'ELLT', 'PRICE': 10}
]

current_buy_threshold = 100
current_worthless_threshold = 0

show_buy = True
show_passes = False
show_worthless = False

is_quit = False

# endregion Variables

# region Widgets

# Root Tk window
tk_root = Tk()

# Display for script activity
activity_display = Text(tk_root, state=DISABLED)
activity_display.pack(fill=BOTH, expand=YES)
activity_display.insert(END, " ")

# Entry box for commands
command_entry = Entry(tk_root)
command_entry.pack(fill=BOTH)


# endregion Widgets

# region Functions


def select_stock(stream):
    # Pretend something is happening here to actually process data and not just
    # get random stuff
    stream.on_next(random.choice(stocks))


def push_message_to_activity(message, stream):
    """Pushes a message to the activity subject.

    TODO: Add some error checking here. make sure we have a valid message maybe?
    """
    stream.on_next(message)


def check_and_publish_stock(stock, stream):
    """Determine if we buy the stock! This really is garbage logic... don't judge me.

    Also pushes the message to the activity subject if the options are set to do so.
    TODO: Make options a dictionary or array... may be easier to test. Not sure how testing works out in Python.
    """
    global current_buy_threshold
    global current_worthless_threshold

    global show_buy
    global show_passes
    global show_worthless

    if stock['PRICE'] >= current_buy_threshold and show_buy:
        push_message_to_activity("Buy stock {} for {}!".format(stock['TCKR'], stock['PRICE']),
                                 stream)
    elif stock['PRICE'] <= current_worthless_threshold and show_worthless:
        push_message_to_activity("{} is worthless at {}!".format(stock['TCKR'], stock['PRICE']),
                                 stream)
    elif show_passes and current_worthless_threshold < stock['PRICE'] < current_buy_threshold:
        push_message_to_activity("Going to pass on {} for now. Price {} is low.".format(stock['TCKR'], stock['PRICE']),
                                 stream)


def update_activity_display(message, display):
    """Adds and updates the message to the supplied display

    :param message: Message to add to activity display
    :param display: The activity display to add text to
    :return: None
    """
    # Display is disabled by default. It must be enabled to add text.
    display.config(state=NORMAL)
    display.insert(END, message + "\n")

    # Scrolls the display to the end, showing the last message.
    display.see(END)

    display.config(state=DISABLED)


def display_timer_info(count, stream):
    global current_buy_threshold
    global current_worthless_threshold

    push_message_to_activity("It has been {} seconds since we started running. "
                             "\nThe current buy threshold is {}."
                             "\nStocks at or below {} are worthless"
                             .format((count + 1) * 30, current_buy_threshold,
                                     current_worthless_threshold),
                             stream)


def process_command(command):
    global current_buy_threshold
    global current_worthless_threshold

    global show_buy
    global show_passes
    global show_worthless

    global is_quit

    split_command = command.split()

    # TODO: This logic was quick and dirty. Probably a better way to implement this.
    if len(split_command) > 0:

        # region Quit Command
        if split_command[0] == "quit":
            is_quit = True
            return "Quitting..."

        # endregion Quit Command

        # region Set Commands
        elif split_command[0] == "set":
            if len(split_command) > 1 and split_command[1] == "buy":
                if len(split_command) > 2 and split_command[2].isdigit():
                    current_buy_threshold = int(split_command[2])
                    return "Updated buy threshold to {}"\
                        .format(split_command[2])
                else:
                    return "Invalid value specified for buy.\n" \
                           "Set command usage example: \"set buy 100\""

            elif len(split_command) > 1 and split_command[1] == "worthless":
                if len(split_command) > 2 and split_command[2].isdigit():
                    current_worthless_threshold = int(split_command[2])
                    return "Updated worthless threshold to {}"\
                        .format(split_command[2])
                else:
                    return "Invalid value specified for worthless.\n" \
                           "Set command usage example: \"set worthless 100\""

            else:
                return "Invalid command.\n" \
                       "Set command usage example: \"set buy 100\"."
        # endregion Set Commands

        # region Show Commands

        elif split_command[0] == "show":
            if len(split_command) > 1 and split_command[1] == "buy":
                if len(split_command) > 2 and split_command[2].lower() == "false":
                    show_buy = False
                    return "Updated show buy to false."
                elif len(split_command) > 2 and split_command[2].lower() == "true":
                    show_buy = True
                    return "Updated show buy to true."
                else:
                    return "Invalid value specified for buy.\n" \
                           "Show command usage example: \"show buy true\""

            elif len(split_command) > 1 and split_command[1] == "passes":
                if len(split_command) > 2 and split_command[2].lower() == "false":
                    show_passes = False
                    return "Updated show passes to false."
                elif len(split_command) > 2 and split_command[2].lower() == "true":
                    show_passes = True
                    return "Updated show passes to true."
                else:
                    return "Invalid value specified for passes.\n" \
                           "Show command usage example: \"show passes true\""

            elif len(split_command) > 1 and split_command[1] == "worthless":
                if len(split_command) > 2 and split_command[2].lower() == "false":
                    show_worthless = False
                    return "Updated show worthless to false."
                elif len(split_command) > 2 and split_command[2].lower() == "true":
                    show_worthless = True
                    return "Updated show worthless to true."
                else:
                    return "Invalid value specified for worthless.\n" \
                           "Show command usage example: \"show worthless true\""

            else:
                return "Invalid command.\n" \
                       "Show command usage example: \"show buy true\"."

        # endregion Show Commands

        # region Help Commands

        # TODO: Implement help commands.

        # endregion Help Commands

        # region Command Not Recognized
        else:
            return "Command not recognized.\n" \
                   "Type \"help\" for list of commands."
        # endregion Command Not Recognized

    # region Command Empty
    else:
        return "Command not specified.\n" \
               "Type \"help\" for list of commands."
    # endregion Command Empty

# region Commands


def return_pressed_command(display, entry):
    global is_quit
    global tk_root

    update_activity_display(process_command(entry.get()), display)

    # Clear the entry after the command has been entered.
    entry.delete(0, END)

    # If we quit, stop the main GUI loop.
    if is_quit:
        tk_root.destroy()

# endregion Commands

# endregion Functions

# region Observables


activity_stream = Subject()
activity_stream.on_next("Input commands anytime using the command input area.")

activity_stream.subscribe(lambda message: update_activity_display(message, activity_display))

stock_stream = Subject()
select_stock(stock_stream)

# Grab whatever our select_stock method thinks is appropriate once it does its 'processing'.
# We emit the result every 1000 ms.
# noinspection PyUnresolvedReferences
stock_selector = Observable.interval(1000) \
    .subscribe(lambda x: select_stock(stock_stream))

# Subscribe to the subject. Subject is passed new stocks through the on_next call.
# Here we are printing the results of the Subject being passed the next item by
# subscribing to the observable and running a lambda when it receives new data.
stock_stream.subscribe(lambda stock: check_and_publish_stock(stock, activity_stream))

# This just acts as a timer. Every thirty seconds we let the user know how long the
# script has been running and the current buy threshold.
# noinspection PyUnresolvedReferences
timer_info = Observable.interval(30000) \
    .subscribe(lambda count: display_timer_info(count, activity_stream))


# endregion Observables

# region Command Bindings


command_entry.bind('<Return>', lambda event: return_pressed_command(activity_display, command_entry))


# endregion Command Bindings

# region Main


def main():
    tk_root.mainloop()

    # Clean up subscriptions and subjects after exiting the GUI loop.
    stock_selector.dispose()
    timer_info.dispose()

    stock_stream.dispose()
    activity_stream.dispose()

# endregion Main


if __name__ == '__main__':
    main()
